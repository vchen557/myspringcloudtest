package wang.vchen.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by vchen on 2017/7/6.
 */
@Controller
public class HelloController {

    @RequestMapping(value = "/")
    @ResponseBody
    public Object sayHelloWorld(){
        return "Hello World";
    }


}
