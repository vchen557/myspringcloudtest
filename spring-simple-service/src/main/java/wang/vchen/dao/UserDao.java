package wang.vchen.dao;

import wang.vchen.model.User;

import java.util.List;



public interface UserDao {

	List<User> findAll();
}
